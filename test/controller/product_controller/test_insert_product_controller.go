package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

func main()  {

	postBody, _ := json.Marshal(map[string]string{
		"name":"new product",
		"price":"1000",
		"description":"desc",
	})
	responseBody := bytes.NewBuffer(postBody)
	//fmt.Println(responseBody)
	//Leverage Go's HTTP Post function to make request
	client := &http.Client{}
	req, _ := http.NewRequest("POST", "http://localhost:8080/insert_product", responseBody)
	req.Header.Add("authorization","")
	resp, err := client.Do(req)
	//Handle Error
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()
	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	sb := string(body)
	log.Printf(sb)
}