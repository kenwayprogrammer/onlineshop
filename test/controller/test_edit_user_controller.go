package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

func main()  {

	postBody, _ := json.Marshal(map[string]string{
		"name":  	"mamad",
		"last_name":"mamad",
		"phone":	"mamad",
		"password": "mamad",
		"username":	"mamad",
	})
	responseBody := bytes.NewBuffer(postBody)
	//Leverage Go's HTTP Post function to make request
	client := &http.Client{}
	req, _ := http.NewRequest("POST", "http://localhost:8080/edit_user", responseBody)
	req.Header.Add("Authorization","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI6Im1hbWFkIiwiZXhwIjo0NjQ5MDI2MDM1fQ.HM-Iao6oRIRJRuKDEv-gD0fl3XeT-wvlBKhuaLzMR3Q")
	resp, err := client.Do(req)
	//Handle Error
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()
	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	sb := string(body)
	log.Printf(sb)
}