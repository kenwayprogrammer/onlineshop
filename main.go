package main

import (
	"online_shop/config"
	"online_shop/repository/mongodb"
	"online_shop/repository/postgres"
	"online_shop/transport/http"
)

func main() {
	mongodb.MongoDbManager{}.ConnectToMongoDB(config.MongoDBURL)
	postgres.PostgresDBManager{}.CreateConnection(config.PostgresDBURL)

	http.RunRouter()

	mongodb.MongoDbManager{}.Disconnect()
	postgres.PostgresDBManager{}.CloseConnection()
}









