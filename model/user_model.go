package model



type User struct {
	Username string `json:"username"  bson:"_id"`
	Password string `json:"password"  bson:"password"`
	Name string 	`json:"name"      bson:"name"`
	LastName string `json:"last_name" bson:"last_name"`
	Phone string 	`json:"phone"     bson:"phone"`
}

func (user User) FromMap(json map[string]string) User{
	return User{
		Username: json["username"],
		Password: json["password"],
		Name: 	  json["name"],
		LastName: json["last_name"],
		Phone:    json["phone"],
	}
}

