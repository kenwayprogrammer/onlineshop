package model


type Product struct {
	Name 		string `json:"name" bson:"name"`
	Price 		string `json:"price" bson:"price"`
	Description string `json:"description" bson:"description"`
}