package config

const (
	MongoDBURL = "mongodb://127.0.0.1:27017"
	MongoDBDatabaseName = "online_shop"
	MongoDBUserCollectionName = "users"
	MongoDBProductsCollectionName = "products"
)

const (
	PostgresDBURL = "postgresql://postgres:Unity1775@localhost:8822/postgres"
	PostgresDatabaseName = "online_shop"
	PostgresUserTable = "users"
	PostgresProductsTable = "products"
)