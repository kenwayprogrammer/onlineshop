package gin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"online_shop/controller"
)

type SignupRequest struct {
	Username 	string `json:"username"`
	Password 	string `json:"password"`
	Name 		string `json:"name"`
	LastName 	string `json:"last_name"`
	Phone 		string `json:"phone"`
}

func (signupRequest SignupRequest) Handle(ginContext *gin.Context){

	ginContext.Header("Content-Type", "application/json")

	bindingBodyError := ginContext.BindJSON(&signupRequest)
	if bindingBodyError != nil {
		fmt.Println(bindingBodyError)
	}

	signupResponse := controller.SingUpController(signupRequest.Username, signupRequest.Password, signupRequest.Name, signupRequest.LastName, signupRequest.Phone)

	ginContext.JSON(signupResponse.Status, signupResponse.Data)
}