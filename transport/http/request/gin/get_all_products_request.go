package gin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"online_shop/controller"
)

type GetAllProductsRequest struct {
	Token 	string `json:"token"`
}

func (getAllProductsRequest GetAllProductsRequest) Handle(ginContext *gin.Context){

	ginContext.Header("Content-Type", "application/json")

	bindingHeaderError := ginContext.BindHeader(&getAllProductsRequest)
	if bindingHeaderError != nil {
		fmt.Println(bindingHeaderError)
	}

	editUserResponse := controller.GetAllProductsController(getAllProductsRequest.Token)

	ginContext.JSON(editUserResponse.Status, editUserResponse.Data)
}