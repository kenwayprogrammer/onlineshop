package gin

import (
	"github.com/gin-gonic/gin"
	"log"
	"online_shop/config"
)

var GinRouter = gin.Default()

func RunGin(){
	err := GinRouter.Run(config.ServerPort)
	if err != nil{
		log.Fatalln(err)
	}
}