package gin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"online_shop/controller"
)

type GetProductByIdRequest struct {
	Id 		string `json:"id"`
	Token 	string `json:"token"`
}

func (getProductByIdRequest GetProductByIdRequest) Handle(ginContext *gin.Context){

	ginContext.Header("Content-Type", "application/json")

	bindingHeaderError := ginContext.BindHeader(&getProductByIdRequest)
	if bindingHeaderError != nil {
		fmt.Println(bindingHeaderError)
	}

	editUserResponse := controller.GetProductByIdController(getProductByIdRequest.Id, getProductByIdRequest.Token)

	ginContext.JSON(editUserResponse.Status, editUserResponse.Data)
}