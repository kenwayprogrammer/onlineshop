package gin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"online_shop/controller"
)

type InsertProductRequest struct {
	Token 	string `json:"token"`
	Name 		string `json:"name" bson:"name"`
	Price 		string `json:"price" bson:"price"`
	Description string `json:"description" bson:"description"`
}

func (insertProductRequest InsertProductRequest) Handle(ginContext *gin.Context){

	ginContext.Header("Content-Type", "application/json")

	bindingHeaderError := ginContext.BindHeader(&insertProductRequest)
	if bindingHeaderError != nil {
		fmt.Println(bindingHeaderError)
	}
	bindingBodyError := ginContext.BindJSON(&insertProductRequest)
	if bindingBodyError != nil {
		fmt.Println(bindingBodyError)
	}

	insertProductResponse := controller.InsertProductController(
		insertProductRequest.Name,
		insertProductRequest.Price,
		insertProductRequest.Description,
		insertProductRequest.Token)

	ginContext.JSON(insertProductResponse.Status, insertProductResponse.Data)
}