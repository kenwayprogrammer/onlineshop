package gin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"online_shop/controller"
	"online_shop/model"
)

type EditUserRequest struct {
	Token 	string `json:"token"`
}

func (editUserRequest EditUserRequest) Handle(ginContext *gin.Context){

	ginContext.Header("Content-Type", "application/json")

	bindingHeaderError := ginContext.BindHeader(&editUserRequest)
	if bindingHeaderError != nil {
		fmt.Println(bindingHeaderError)
	}

	userModel := model.User{}
	bindingBodyError := ginContext.BindJSON(&userModel)
	if bindingBodyError != nil {
		fmt.Println(bindingBodyError)
	}

	editUserResponse := controller.EditUserController(userModel, editUserRequest.Token)

	ginContext.JSON(editUserResponse.Status, editUserResponse.Data)
}