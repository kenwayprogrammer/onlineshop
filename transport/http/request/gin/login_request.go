package gin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"online_shop/controller"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (loginRequest LoginRequest) Handle(ginContext *gin.Context){

	ginContext.Header("Content-Type", "application/json")

	bindingBodyError := ginContext.BindJSON(&loginRequest)
	if bindingBodyError != nil {
		fmt.Println(bindingBodyError)
	}

	loginResponse := controller.LoginController(loginRequest.Username,loginRequest.Password)

	ginContext.JSON(loginResponse.Status, loginResponse.Data)
}