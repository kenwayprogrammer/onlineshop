package response

import "net/http"

type SignupResponse struct {
	Data map[string]interface{}
	Status int
}

func (signupResponse SignupResponse) Successful() SignupResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = true
	dataResponse["message"] = "Signup was successful"
	return SignupResponse{dataResponse,http.StatusOK}
}

func (signupResponse SignupResponse) UserIsExistError() SignupResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "User is exist currently"
	return SignupResponse{dataResponse,http.StatusUnauthorized}
}

func (signupResponse SignupResponse) BadRequestError() SignupResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "Bad Request"
	return SignupResponse{dataResponse,http.StatusBadRequest}
}

func (signupResponse SignupResponse) InternalServerError() SignupResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "An internal error occur"
	return SignupResponse{dataResponse,http.StatusInternalServerError}
}
