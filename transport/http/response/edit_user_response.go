package response

import "net/http"

type EditUserResponse struct {
	Data map[string]interface{}
	Status int
}

func (editUserResponse EditUserResponse) Successful() EditUserResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = true
	dataResponse["message"] = "Signup was successful"
	return EditUserResponse{dataResponse,http.StatusOK}
}

func (editUserResponse EditUserResponse) UserNotExistError() EditUserResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "This user does not exist"
	return EditUserResponse{dataResponse,http.StatusUnauthorized}
}

func (editUserResponse EditUserResponse) BadRequestError() EditUserResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "Bad Request"
	return EditUserResponse{dataResponse,http.StatusBadRequest}
}

func (editUserResponse EditUserResponse) InternalServerError() EditUserResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "An internal error occur"
	return EditUserResponse{dataResponse,http.StatusInternalServerError}
}
