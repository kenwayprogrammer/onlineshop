package response

import "net/http"

type GetAllProductsResponse struct {
	Data map[string]interface{}
	Status int
}

func (getAllProductsResponse GetAllProductsResponse) Successful(products []interface{}) GetAllProductsResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = true
	dataResponse["message"] = ""
	dataResponse["products"] = products
	return GetAllProductsResponse{dataResponse,http.StatusOK}
}

func (getAllProductsResponse GetAllProductsResponse) UserNotExistError() GetAllProductsResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "This user does not exist"
	return GetAllProductsResponse{dataResponse,http.StatusUnauthorized}
}

func (getAllProductsResponse GetAllProductsResponse) BadRequestError() GetAllProductsResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "Bad Request"
	return GetAllProductsResponse{dataResponse,http.StatusBadRequest}
}

func (getAllProductsResponse GetAllProductsResponse) InternalServerError() GetAllProductsResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "An internal error occur"
	return GetAllProductsResponse{dataResponse,http.StatusInternalServerError}
}
