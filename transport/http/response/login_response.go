package response

import "net/http"

type LoginResponse struct {
	Data map[string]interface{}
	Status int
}

func (loginResponse LoginResponse) Successful(data interface{}) LoginResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = true
	dataResponse["message"] = "Login was successful"
	dataResponse["token"] = data
	return LoginResponse{dataResponse,http.StatusOK}
}

func (loginResponse LoginResponse) UserNotExistError() LoginResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "This user does not exist"
	dataResponse["token"] = nil
	return LoginResponse{dataResponse,http.StatusUnauthorized}
}

func (loginResponse LoginResponse) WrongPasswordError() LoginResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "Password is incorrect"
	return LoginResponse{dataResponse,http.StatusUnauthorized}
}

func (loginResponse LoginResponse) BadRequestError() LoginResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "Bad Request"
	return LoginResponse{dataResponse,http.StatusBadRequest}
}

func (loginResponse LoginResponse) InternalServerError() LoginResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "An internal error occur"
	return LoginResponse{dataResponse,http.StatusInternalServerError}
}
