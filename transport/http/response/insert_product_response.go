package response

import "net/http"

type InsertProductResponse struct {
	Data map[string]interface{}
	Status int
}

func (insertProductResponse InsertProductResponse) Successful() InsertProductResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = true
	dataResponse["message"] = "Signup was successful"
	return InsertProductResponse{dataResponse,http.StatusOK}
}


func (insertProductResponse InsertProductResponse) BadRequestError() InsertProductResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "Bad Request"
	return InsertProductResponse{dataResponse,http.StatusBadRequest}
}

func (insertProductResponse InsertProductResponse) InternalServerError() InsertProductResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "An internal error occur"
	return InsertProductResponse{dataResponse,http.StatusInternalServerError}
}
