package response

import (
	"net/http"
	"online_shop/model"
)

type GetProductByIdResponse struct {
	Data map[string]interface{}
	Status int
}

func (getProductByIdResponse GetProductByIdResponse) Successful(product model.Product) GetProductByIdResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = true
	dataResponse["message"] = ""
	dataResponse["products"] = product
	return GetProductByIdResponse{dataResponse,http.StatusOK}
}

func (getProductByIdResponse GetProductByIdResponse) ProductNotExistError() GetProductByIdResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "This product does not exist"
	return GetProductByIdResponse{dataResponse,http.StatusUnauthorized}
}

func (getProductByIdResponse GetProductByIdResponse) BadRequestError() GetProductByIdResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "Bad Request"
	return GetProductByIdResponse{dataResponse,http.StatusBadRequest}
}

func (getProductByIdResponse GetProductByIdResponse) InternalServerError() GetProductByIdResponse {
	dataResponse := make(map[string]interface{})
	dataResponse["success"] = false
	dataResponse["message"] = "An internal error occur"
	return GetProductByIdResponse{dataResponse,http.StatusInternalServerError}
}
