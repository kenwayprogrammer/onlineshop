package http

import (
	"online_shop/transport/http/request/gin"
)

func RunRouter(){
	gin.GinRouter.POST("/login", gin.LoginRequest{}.Handle)
	gin.GinRouter.POST("/signup", gin.SignupRequest{}.Handle)
	gin.GinRouter.POST("/edit_user", gin.EditUserRequest{}.Handle)
	gin.GinRouter.GET("/get_all_products", gin.GetAllProductsRequest{}.Handle)
	gin.GinRouter.GET("/get_product_by_id", gin.GetProductByIdRequest{}.Handle)
	gin.GinRouter.POST("/delete_product", gin.GetAllProductsRequest{}.Handle)
	gin.GinRouter.POST("/insert_product", gin.InsertProductRequest{}.Handle)
	gin.RunGin()
}