package utils

func StringsIsEmpty(strings[]string) bool{
	for _ , item := range strings{
		if len(item) == 0 || item == "null" || item == "" || item == "nil"{
			return true
		}
	}

	return false
}
