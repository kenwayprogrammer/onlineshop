package utils

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)


type Claims struct {
	Username string
	jwt.StandardClaims
}

var jwtKey = []byte("hdasdkcoelm3932m1jhd9893mqzpaasdq8098dsakd0932i1ek1asdudjd30921912ndymncnbsaatwqpokikjfiekdk0921lewnzaiza2ejhgddxll9812nt1aa611092ks78122io7812")

func JWTInit(username string)(string,error){
	expTime := time.Now().Add(50000000 * time.Minute)
	cliams := &Claims{}
	cliams = &Claims{
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, cliams)
	stringToken, err := token.SignedString(jwtKey)
	return stringToken, err
}

func JWTValidation(tokenString string)(bool,error,string){
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString,claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil{
		return false , err,claims.Username
	}
	if !token.Valid == true{
		return false , err,claims.Username
	}
	return true , err,claims.Username
}