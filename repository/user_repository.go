package repository

import (
	"online_shop/model"
	"online_shop/repository/mongodb"
)

type UserRepository struct {}

func (userRepository UserRepository) IsUserExist(username string) model.User{
	return mongodb.UserCollectionObject.FindOne(username)
}

func (userRepository UserRepository) AddUser(user model.User)error{
	err := mongodb.UserCollectionObject.InsertOne(user)
	return err
}

func (userRepository UserRepository) EditUser(user model.User) error{
	err := mongodb.UserCollectionObject.UpdateOne(user)
	return err
}