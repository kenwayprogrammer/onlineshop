package postgres

import (
	"fmt"
	"log"
	"online_shop/model"
)

type UserTable struct {}

func (userTable UserTable) insertUser(user model.User) int64{

	// create the insert sql query
	// returning userid will return the id of the inserted user
	sqlStatement := `INSERT INTO users (username, password, name, last_name, phone) VALUES ($1, $2, $3, $4, $5,) RETURNING userid`

	// the inserted id will store in this id
	var id int64

	// execute the sql statement
	// Scan function will save the insert id in the id
	err := PostgresDB.db.QueryRow(sqlStatement, user.Username, user.Password, user.Name, user.LastName, user.Phone,).Scan(&id)

	if err != nil {
		log.Fatalf("Unable to execute the query. %v", err)
	}

	fmt.Printf("Inserted a single record %v", id)

	// return the inserted id
	return id
}