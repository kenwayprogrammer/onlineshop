package postgres

import (
	"database/sql"
	"log"
)

type PostgresDBManager struct {
	db *sql.DB
}

var PostgresDB = &PostgresDBManager{}

func (postgresDBManager PostgresDBManager) CreateConnection(dbURL string){

	// Open the connection
	db, err := sql.Open("postgres", dbURL)

	if err != nil {
		log.Fatalln(err)
	}

	// check the connection
	err = db.Ping()

	if err != nil {
		log.Fatalln(err)
	}

	PostgresDB.db = db
}


func (postgresDBManager PostgresDBManager) CloseConnection(){
	err := PostgresDB.db.Close()
	if err != nil {
		log.Fatalln(err)
	}
}