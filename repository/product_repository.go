package repository

import (
	"context"
	"online_shop/model"
	"online_shop/repository/mongodb"
)

type ProductRepository struct {}

func (productRepository ProductRepository) GetProductById(productId string) (model.Product, error) {
	return mongodb.ProductCollectionObject.FindOne(productId)
}

func (productRepository ProductRepository) GetAllProducts() ([]interface{},error){
	listFromDB,err := mongodb.ProductCollectionObject.FindAll()
	var finalList []interface{}
	for listFromDB.Next(context.Background()){
		finalList = append(finalList,listFromDB.Current.String())
	}
	return finalList,err
}

func (productRepository ProductRepository) InsertProduct(product model.Product) error {
	err := mongodb.ProductCollectionObject.InsertOne(product)
	return err
}