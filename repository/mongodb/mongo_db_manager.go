package mongodb

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"online_shop/config"
	"time"
)

type MongoDbManager struct {
	DbConnection  *mongo.Client
}

//var MongoManager = &MongoDbManager{}

func (mongoDbManager MongoDbManager) ConnectToMongoDB(dbURL string){
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(dbURL))
	if err != nil {
		fmt.Println(err)
	}
	err = client.Ping(ctx, nil)
	if err != nil {
		fmt.Println(err)
	}
	mongoDbManager.DbConnection = client
	// initial collections instances
	UserCollectionObject.Instance 	 = mongoDbManager.GetCollection(config.MongoDBDatabaseName,config.MongoDBUserCollectionName)
	ProductCollectionObject.Instance = mongoDbManager.GetCollection(config.MongoDBDatabaseName,config.MongoDBProductsCollectionName)
}

func (mongoDbManager MongoDbManager) Disconnect(){
	err := mongoDbManager.DbConnection.Disconnect(context.Background())
	if err != nil {
		fmt.Println(err)
	}
}

func (mongoDbManager MongoDbManager) GetCollection (dbName string,collectionName string) *mongo.Collection {
	collection := mongoDbManager.DbConnection.Database(dbName).Collection(collectionName)
	return collection
}

