package mongodb

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"online_shop/model"
)



type UserCollection struct {
	Instance *mongo.Collection
}


var UserCollectionObject = &UserCollection{}


func (userCollection *UserCollection) InsertOne (userModel model.User)error{
	_, err := userCollection.Instance.InsertOne(context.Background(), userModel)
	return err
}


func (userCollection *UserCollection) UpdateOne (userModel model.User)error{
	_ , err := userCollection.Instance.UpdateOne(context.Background(),
		userModel.Username,
		userModel,
	)
	return err
}


func (userCollection UserCollection) FindOne (username string) model.User{
	userModel := model.User{}
	err := userCollection.Instance.FindOne(context.Background(), bson.M{"_id":username}).Decode(&userModel)
	if err != nil {
		// ErrNoDocuments means that the filter did not match any documents in the collection
		if err == mongo.ErrNoDocuments {
			return userModel
		}
	}
	return userModel
}


func (userCollection UserCollection) FindAll() *mongo.Cursor{
	list ,err := userCollection.Instance.Find(context.Background(),bson.D{{}})
	if err != nil{
		fmt.Println(err)
	}
	return list
}

