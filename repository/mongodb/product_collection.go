package mongodb

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"online_shop/model"
)



type ProductCollection struct {
	Instance *mongo.Collection
}

var ProductCollectionObject = &ProductCollection{}

func (productCollection *ProductCollection) InsertOne (product model.Product)error{
	_ , err := productCollection.Instance.InsertOne(context.Background(),
		bson.M{
			product.Name:product.Name,
			product.Price:product.Price,
			product.Description:product.Description})
	return err
}

func (productCollection ProductCollection) FindOne (id string) (model.Product, error){
	productModel := model.Product{}
	err := productCollection.Instance.FindOne(context.Background(), bson.M{"_id":id}).Decode(&productModel)
	if err != nil {
		// ErrNoDocuments means that the filter did not match any documents in the collection
		if err == mongo.ErrNoDocuments {
			return productModel, err
		}
	}
	return productModel, nil
}

func (productCollection ProductCollection) FindAll() (*mongo.Cursor,error){
	list ,err := productCollection.Instance.Find(context.Background(),bson.D{{}})
	if err != nil{
		fmt.Println(err)
	}
	return list,err
}

func (productCollection ProductCollection) DeleteOne(filter bson.D) bool{
	_,err := productCollection.Instance.DeleteOne(context.Background(), filter)
	if err != nil {
		// ErrNoDocuments means that the filter did not match any documents in the collection
		if err == mongo.ErrNoDocuments {
			return false
		}
		log.Fatal(err)
	}
	fmt.Printf("deleted documents by filter %v", filter)
	return true
}

