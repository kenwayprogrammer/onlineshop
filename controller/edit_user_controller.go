package controller

import (
	"github.com/dgrijalva/jwt-go"
	"online_shop/model"
	"online_shop/repository"
	"online_shop/transport/http/response"
	"online_shop/utils"
)

func EditUserController(userModel model.User, token string) response.EditUserResponse{

	//token recognizing/////////////////////////////////////
	tokenValidation, tokenError, username := utils.JWTValidation(token)
	if !tokenValidation || username != userModel.Username{
		return response.EditUserResponse{}.UserNotExistError()
	}
	if tokenError != nil {
		if tokenError == jwt.ErrSignatureInvalid {}
		return response.EditUserResponse{}.InternalServerError()
	}


	//edit user in repository
	userRepository := repository.UserRepository{}
	userEditionError := userRepository.EditUser(userModel)
	if userEditionError != nil {
		return response.EditUserResponse{}.InternalServerError()
	}


	return response.EditUserResponse{}.Successful()
}
