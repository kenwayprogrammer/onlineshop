package controller

import (
	"online_shop/model"
	"online_shop/repository"
	"online_shop/transport/http/response"
	"online_shop/utils"
)

func SingUpController(
		username string,
		password string,
		name     string,
		lastname string,
		phone 	 string,
	) response.SignupResponse{


	if utils.StringsIsEmpty([]string{username,password,name,lastname,phone}){
		return response.SignupResponse{}.BadRequestError()
	}

	userModel := model.User{Username: username, Password:password, Name: name, LastName:lastname, Phone:phone}

	userRepository := &repository.UserRepository{}
	insertionError := userRepository.AddUser(userModel)
	if insertionError != nil{
		return response.SignupResponse{}.UserIsExistError()
	}


	return response.SignupResponse{}.Successful()

}
