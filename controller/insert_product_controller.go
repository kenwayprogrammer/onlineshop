package controller

import (
	"github.com/dgrijalva/jwt-go"
	"online_shop/model"
	"online_shop/repository"
	"online_shop/transport/http/response"
	"online_shop/utils"
)

func InsertProductController(name string,price string, description string, token string) response.InsertProductResponse{

	//token recognizing/////////////////////////////////////
	tokenValidation, tokenError, _ := utils.JWTValidation(token)
	if !tokenValidation {
		return response.InsertProductResponse{}.BadRequestError()
	}
	if tokenError != nil {
		if tokenError == jwt.ErrSignatureInvalid {}
		return response.InsertProductResponse{}.InternalServerError()
	}


	//insert product into repository
	productRepository := repository.ProductRepository{}
	insertProductError := productRepository.InsertProduct(
		model.Product{
			Name: name,
			Price: price,
			Description: description,
		})
	if insertProductError != nil {
		return response.InsertProductResponse{}.InternalServerError()
	}


	return response.InsertProductResponse{}.Successful()
}
