package controller

import (
	"github.com/dgrijalva/jwt-go"
	"online_shop/repository"
	"online_shop/transport/http/response"
	"online_shop/utils"
)

func GetAllProductsController(token string) response.GetAllProductsResponse{


	//token recognizing/////////////////////////////////////
	tokenValidation, tokenError, _ := utils.JWTValidation(token)
	if !tokenValidation {
		return response.GetAllProductsResponse{}.UserNotExistError()
	}
	if tokenError != nil {
		if tokenError == jwt.ErrSignatureInvalid {}
		return response.GetAllProductsResponse{}.InternalServerError()
	}

	//get all products from repository
	productRepository := repository.ProductRepository{}
	products, getProductsError := productRepository.GetAllProducts()
	if getProductsError != nil {
		return response.GetAllProductsResponse{}.InternalServerError()
	}


	return response.GetAllProductsResponse{}.Successful(products)
}
