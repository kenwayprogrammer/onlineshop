package controller

import (
	"online_shop/repository"
	"online_shop/transport/http/response"
	"online_shop/utils"
)

func LoginController(username string, password string) response.LoginResponse{


	if utils.StringsIsEmpty([]string{username,password}){
		return response.LoginResponse{}.BadRequestError()
	}
	////////////////////////////////////////////////////////


	//check user from repository
	userRepo := &repository.UserRepository{}
	userModel := userRepo.IsUserExist(username)
	if userModel.Username == ""{
		return response.LoginResponse{}.UserNotExistError()
	}
	if userModel.Password != password {
		return response.LoginResponse{}.WrongPasswordError()
	}
	////////////////////////////////////////////////////////


	//Configure  JWT Token////////////////////////////////////
	token,tokenErr := utils.JWTInit(username)
	if tokenErr != nil {
		return response.LoginResponse{}.InternalServerError()
	}
	/////////////////////////////////////////////////////////////

	return response.LoginResponse{}.Successful(token)

}

