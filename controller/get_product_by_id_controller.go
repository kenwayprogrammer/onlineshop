package controller

import (
	"github.com/dgrijalva/jwt-go"
	"online_shop/repository"
	"online_shop/transport/http/response"
	"online_shop/utils"
)

func GetProductByIdController(productId string, token string) response.GetProductByIdResponse{

	//token recognizing/////////////////////////////////////
	tokenValidation, tokenError, _ := utils.JWTValidation(token)
	if !tokenValidation {
		return response.GetProductByIdResponse{}.BadRequestError()
	}
	if tokenError != nil {
		if tokenError == jwt.ErrSignatureInvalid {}
		return response.GetProductByIdResponse{}.InternalServerError()
	}


	//get product by id from repository
	productRepository := repository.ProductRepository{}
	product, getProductError := productRepository.GetProductById(productId)
	if getProductError != nil {
		return response.GetProductByIdResponse{}.InternalServerError()
	}


	return response.GetProductByIdResponse{}.Successful(product)
}
