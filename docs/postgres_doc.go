package docs


/*

Author : Mohamad Reza Ghanadan

Postgres service commands in ubuntu :
sudo service postgresql start
sudo service postgresql stop
sudo service postgresql status

Connection to terminal :
sudo -i -u postgres
psql
psql

Help command :
help

Listing all databases :
\l

Creating database :
create database db_name;

Connecting to a database by name :
\c db_name

After connecting to a database
for listing all tables :
\d

For seeing table detail :
\d table_name


*/
